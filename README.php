<?php
	private function syncParticipants($objDom) {
		assert(!empty($objDom));
		
		// Get the project key from XML and get the associated project model
		$projectKeyList = $objDom->getElementsByTagName('Project');
		if ($projectKeyList->length==0) {
			$this->addToResults('ProjectKey', '');			
			return $this->prepareReturn(false, 'ERROR: No ProjectKey found');
		}
		$projectKey = $projectKeyList->item(0)->nodeValue;
		$this->addToResults('ProjectKey', $projectKey);
		$this->log->set_request_project_key($projectKey);
		
	  	if (empty($projectKey)) {
			return $this->prepareReturn(false, 'ERROR: Empty project_key'); 
	  	}
	  	
	  	if (
	  		strpos($projectKey, "'") // single quote
	  		|| strpos($projectKey, '"') // double quote
	  		|| strpos($projectKey, '`') // reversed single quote
	  		|| strpos($projectKey, '&')
	  		|| strpos($projectKey, ';')
	  		|| strpos($projectKey, ' ')
	  		|| strpos($projectKey, '$')
	  		|| strpos($projectKey, '/')
	  		|| strpos($projectKey, '\\')
	  	) {
			return $this->prepareReturn(false, 'ERROR: Invalid project_key'); 
	  	}

		$projects = find("Projects", array('sync_key'=>$projectKey));
		if(empty($projects)) {
			return $this->prepareReturn(false, 'ERROR: Non existing project_key'); 
		}
		if (count($projects)>1) {
			return $this->prepareReturn(false, 'ERROR: Duplicate project_key'); 
		}
		$project = $projects[0];

        // store project in log
        $this->log->set_project_id($project->get_id());

		// Validate access for the the IP address the request originated from
		$syncAccessIPs = $project->get_sync_access_ips();
		if (empty($syncAccessIPs)) {
			return $this->prepareReturn(false, 'ERROR: Access denied for IP ('.$this->remoteAddr.') on this project (' . $project->get_sync_key() . ') because the access list is empty'); 
		}
		$syncAccessIPs = explode('|', $syncAccessIPs);
		if (!in_array($this->remoteAddr, $syncAccessIPs)) {
			return $this->prepareReturn(false, 'ERROR: Access denied for IP ('.$this->remoteAddr.') on this project (' . $project->get_sync_key() . ')'); 
		}

		// Get the participant data from the XML and store them in an existing or new participant model
		$participantsNodeList = $objDom->getElementsByTagName('Participants');
		$participantNodeList = $participantsNodeList->item(0)->getElementsByTagName('Participant');
		
		for($i=0; $i<$participantNodeList->length; $i++) {
		
			$participantElement = $participantNodeList->item($i);
			$bsn = $this->getElementValue($participantElement, 'BSN');
			$participants = Model::findEncrypted('Participant', array('bsn' => $bsn, 'account_id' => $project->get_account_id()));
			if (!empty($participants)) {
				$participant = $participants[0];
			} else {
				$participant = new Participant();
			}
				
			$contactpersoonRoepnaam = $this->getElementValue($participantElement, 'ContactpersoonRoepnaam');
			if ($contactpersoonRoepnaam != '') {
				$contactpersoonRoepnaam = '(' . $contactpersoonRoepnaam . ')';
			}
		
			$contactpersoon = implode(' ', array(
					$this->getElementValue($participantElement, 'ContactpersoonVoorletters'),
					$contactpersoonRoepnaam,
					$this->getElementValue($participantElement, 'ContactpersoonTvAchternaam'),
					$this->getElementValue($participantElement, 'ContactpersoonAchternaam')));
		
			$geboortedatumString = $this->getElementValue($participantElement, 'Geboortedatum');
		
			$uitkeringBeginString = $this->getElementValue($participantElement, 'TrajectBegindatum');
			$uitkeringBegin = strtotime($uitkeringBeginString);
			if ($uitkeringBegin == -62169985172) { // Ranzige workaround, omdat strtotime('0000-00-00') == '' op ontwikkel, maar strtotime('0000-00-00') == -62169985172 op server, dus hier gelijktrekken.
				$uitkeringBegin = '';
			}
				
			$uitkeringEndString = $this->getElementValue($participantElement, 'TrajectEinddatum');
			$uitkeringEnd = strtotime($uitkeringEndString);
			if ($uitkeringEnd == -62169985172) { // Ranzige workaround, omdat strtotime('0000-00-00') == '' op ontwikkel, maar strtotime('0000-00-00') == -62169985172 op server, dus hier gelijktrekken.
				$uitkeringEnd = '';
			}
				
			$now = strtotime('now');
				
			$participant->update_attributes(array(
					'bsn' => $bsn,
					'registratienummer' => $this->getElementValue($participantElement, 'Clientnummer'),
					'account_id' => $project->get_account_id(),
					'geslacht' => strtoupper($this->getElementValue($participantElement, 'Geslacht')),
					'voorletters' => $this->getElementValue($participantElement, 'Voorletters'),
					'voornaam' => $this->getElementValue($participantElement, 'Voornamen'),
					'volledigenaam' => implode(' ', array(
							$this->getElementValue($participantElement, 'Tussenvoegsel'),
							$this->getElementValue($participantElement, 'Achternaam'))),
					'geboortedatum' => $this->preSaveDateTransformation( $geboortedatumString ),
					'telefoon' => implode(' ', array(
							$this->getElementValue($participantElement, 'Telefoon'),
							$this->getElementValue($participantElement, 'MobielTelefoonnr'))),
					'adres' => implode(' ', array(
							$this->getElementValue($participantElement, 'Straat'),
							$this->getElementValue($participantElement, 'Huisnummer'),
							$this->getElementValue($participantElement, 'ToevoegselBijHuisnr'))),
					'postcode' => $this->getElementValue($participantElement, 'Postcode'),
					'plaats' =>  $this->getElementValue($participantElement, 'Plaatsnaam'),
					'gemeente' =>  $this->getElementValue($participantElement, 'Gemeente'),
					'contactpersoon' => $contactpersoon,
					'uitkering_type' => $this->getElementValue($participantElement, 'Trajectsoort'),
					'uitkering_begin' => $this->preSaveDateTransformation( $uitkeringBeginString ),
					'uitkering_einde' => $this->preSaveDateTransformation( $uitkeringEndString ),
					'werkgever_contactpersoon' => $this->getElementValue($participantElement, 'WerkgeverContactpersoon')
			));
				
			$participant->save();
			if ($participant->has_id()) { // als geen id, dan is de save dus mislukt, bijv  [errors] => Array([bsn] => Voldoet niet aan de elfproef)
		
				//Een deelnemer heeft een actieve uitkering als aan de volgende voorwaarden wordt voldaan:
					
				//TrajectBegindatum is niet leeg
				//EN
				//TrajectBegindatum ligt voor de huidige datum
				//EN
				//(
				//	TrajectEinddatum is leeg
				//	OF
				//	TrajectEinddatum is niet leeg EN TrajectEinddatum ligt na de huidige datum
				//)
		
				$indActieveUitkering =
				(!empty($uitkeringBeginString))
				&& ($uitkeringBegin < $now)
				&& (
						(!isset($uitkeringEnd))
						|| empty($uitkeringEnd)
						|| (empty($uitkeringEndString))
						||
						(
								(!empty($uitkeringEndString))
								&&
								($uitkeringEnd > $now)
						)
				);
					
				$projectsParticipantsXrefs = Model::find('ProjectsParticipantsXref', array('project_id' => $project->get_id(), 'participant_id' => $participant->get_id()));
				if (empty($projectsParticipantsXrefs)) {
					if ($indActieveUitkering) {
						$projectsParticipantsXref = new ProjectsParticipantsXref(array(
								'project_id' => $project->get_id(), 'participant_id' => $participant->get_id(), 'active' => 1
						));
						$projectsParticipantsXref->save();
					}
				} else {
					if (!$indActieveUitkering) {
						$projectsParticipantsXref = $projectsParticipantsXrefs[0];
						$projectsParticipantsXref->delete();
					}
				}

                // save the participant in the log, this will create participant_soap_log_xref records
                $this->log->addParticipantRefToSave($participant);

			} else { // Apparently the participant failed to be saved, return the errors to the caller
				$errors = '';
				foreach($participant->errors as $fieldname => $error) {
					$errors .= $fieldname . ':' . $error . '. ';
				}
				return $this->prepareReturn(false, $errors);
			}
		}		
		
		return $this->prepareReturn(true, 'SUCCESS');		
	}